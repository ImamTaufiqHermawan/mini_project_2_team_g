const router = require('express').Router();
const userController = require('./controller/userController');
const profileController = require('./controller/profileController');

const authenticate = require('./middlewares/authenticate');
const isAdmin = require('./middlewares/isAdmin');
const checkOwnership = require('./middlewares/checkOwnership');
const upload = require('./middlewares/uploader');

router.post('/user/register', upload.single('image'), userController.register);
router.post('/user/login', userController.login);
router.put('/user/updatePassword', authenticate, userController.UpdatePassword);
router.put('/user',authenticate, upload.single('image'),profileController.updateProfile);
router.get('/user', authenticate, userController.userData);

module.exports = router;