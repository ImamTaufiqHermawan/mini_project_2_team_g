const multer = require('multer');
const config = require('../../Mini_Project2/config/uploader');
const env = process.env.NODE_ENV || 'development';
const upload = multer();

module.exports = upload;