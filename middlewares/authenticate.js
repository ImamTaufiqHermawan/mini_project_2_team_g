const jwt = require('jsonwebtoken');
const { user } = require('../models');

module.exports = async (req, res, next) => {
  try {
    let token = req.headers.authorization;
    let payload = await jwt.verify(token, process.env.SECRET_KEY);
    req.user = await user.findByPk(payload.id);
    next();
  }
  catch {
    res.status(401);
    next(new Error("Invalid Token"))
  }
}