const { user, profile } = require('../models');
const bcrypt = require('bcryptjs');
const jwt = require ('jsonwebtoken');

module.exports = {
    async register(req, res) {
        const hashedPassword = await bcrypt.hash(req.body.password, 10);
        try {
            const newUser = await user.create({
                email: req.body.email,
                encrypted_password: hashedPassword,
                role: req.body.role
            })
            const newUserProfile = await profile.create({
                name: req.body.name,
                user_id: newUser.id
            })
            const token = await jwt.sign({
                id: newUser.id,
                email: newUser.email
            }, process.env.SECRET_KEY)
            res.status(200).json({
                "status": "success",
                data: {newUser, newUserProfile, token}
            })
        } 
        catch (err) {
            console.log(err)
            res.status(400).json({
              "status": "failed",
              "message": [err.message]
            })
        }
    },
    async login(req, res) {
        let User = await user.findOne({
            where:{
                email:req.body.email 
            }, include: [profile]
        })
        if (User == null) {
            return res.status(400).json({
                "status": "failed",
                "message": "user doesn't exist or email must be fill with lowercase"
            })
        }
        const token = await jwt.sign({
            id: User.id,
            email: User.email
          }, process.env.SECRET_KEY)
        try {
            if(await bcrypt.compare(req.body.password, User.encrypted_password)) {
                res.status(201).json ({
                    "status": "success",
                    data: {User, token}
                }) 
            } else {
                res.status(400).json ({
                    "status": "failed",
                    "message": "wrong password"
                })
            }
        } 
        catch (err) {
            res.status(500).json({
              "status": "failed",
              "message": [err.message]
          })
        }
    },
    async userData(req, res) {
        try {
            let userData = await user.findByPk(req.user.id, {
                include: [profile]
            });
            res.status(200).json ({
                "status": "Success",
                data: {userData}
            }) 
        } catch (err) {
            res.status(500).json({
                status: 'fail',
                message: err
            })
        }
    },
    async UpdatePassword(req, res) {
        try {
            let userID = req.user.id

            if(req.body.password != null)
                req.body.encrypted_password = bcrypt.hashSync((req.body.password).toString(),10)

            // Update data
            await user.update(req.body,{
                where: {
                    id:userID
                }
            })
            res.status(200).json ({
                "status": "success",
                "message": `successfully update password user ${userID}`
            })              
        } catch (err) {        
            res.status(422).json({
                status: "fail",
                message: [err.message]
            })
        } 
    }
}