'use strict';
module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: {
        args: true,
        msg: 'Email already registered'
      },
      validate: {
        isLowercase: true,
        isEmail: {
          msg: 'Email is invalid'
        },
      }
    },
    encrypted_password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    role: {
      type: DataTypes.ENUM('admin', 'member'),
      defaultValue: 'member',
      allowNull: false
    }
  }, {});
  user.associate = function(models) {
    // associations can be defined here
    user.hasOne(models.profile, {
      foreignKey: 'user_id'
    })
  };
  return user;
};